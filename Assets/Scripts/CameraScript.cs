﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    private float _Speed;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(
            this.transform.position.x + (_Speed * Time.deltaTime), 
            this.transform.position.y, 
            this.transform.position.z);
    }

    public void Stop()
    {
        this._Speed = 0.0f;
    }
}
