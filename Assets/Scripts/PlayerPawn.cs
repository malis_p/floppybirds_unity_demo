﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : MonoBehaviour
{
    [SerializeField]
    private CameraScript    _Camera;

    [SerializeField]
    private float           _JumpSpeed;

    private Rigidbody       _Gravity;
    // Start is called before the first frame update
    void Start()
    {
        this._Gravity = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(this._Camera.transform.position.x, this.transform.position.y, this.transform.position.z);
    }

    public void Jump()
    {
        this._Gravity.AddForce(new Vector3(0, this._JumpSpeed, 0), ForceMode.Impulse);
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Block")
        {
            Debug.Log("GAME OVER");
            EngineManager.instance.GameOver();
        }
        else if (collision.gameObject.tag == "End")
        {
            Debug.Log("LEVEL COMPLETED");
            EngineManager.instance.GameCompleted();
        }
    }
}
