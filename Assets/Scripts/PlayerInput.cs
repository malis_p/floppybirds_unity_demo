﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    private PlayerPawn      _Pawn;

    [SerializeField]
    private bool            _CanJump;

    [SerializeField]
    private Button          _Button;
    // Start is called before the first frame update
    void Start()
    {
        this._CanJump = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (this._CanJump == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Action();
            }
            else if (Input.GetMouseButtonDown(0))
            {
                Action();
            }
            else if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    Action();
                }
                
            }

        }

    }

    public void Action()
    {
        StartCoroutine(ActionCoRoute());
    }

    // WAIT 1/3 SECONDS
    IEnumerator ActionCoRoute()
    {
        this._Button.interactable = false;
        yield return new WaitForSeconds(0.35f);
        this._Pawn.Jump();
        this._CanJump = true;
        this._Button.interactable = true;
    }

    public void Stop()
    {
        this._CanJump = false;
    }

}
