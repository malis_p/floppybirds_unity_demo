﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineManager : MonoBehaviour
{
    public static EngineManager instance = null;

    public GameObject                       _SuccesPanel;
    public GameObject                       _ErrorPanel;
    public CameraScript                     _Cam;
    public PlayerInput                      _Input;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void LoadLevel()
    {
        Application.LoadLevel("Level");
    }

    public void GameCompleted()
    {
        _Input.Stop();
        this._SuccesPanel.SetActive(true);
        _Cam.Stop();
    }

    public void GameOver()
    {
        _Input.Stop();
        this._ErrorPanel.SetActive(true);
        _Cam.Stop();
    }
}
